package top.hmtools.wxmp.user.apis;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;
import top.hmtools.wxmp.user.model.UserListParam;
import top.hmtools.wxmp.user.model.UserListResult;

public class IUserListApiTest {
	
	protected WxmpSession wxmpSession;
	private IUserListApi iUserListApi ;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		iUserListApi = this.wxmpSession.getMapper(IUserListApi.class);
	}

	@Test
	public void testGetUserList() {
		UserListParam userListParam = new UserListParam();
		UserListResult userList = this.iUserListApi.getUserList(userListParam);
		System.out.println(userList);
	}

}
