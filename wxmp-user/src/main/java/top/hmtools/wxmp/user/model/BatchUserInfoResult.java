package top.hmtools.wxmp.user.model;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class BatchUserInfoResult extends ErrcodeBean {

	private List<UserInfoResult> user_info_list;

	public List<UserInfoResult> getUser_info_list() {
		return user_info_list;
	}

	public void setUser_info_list(List<UserInfoResult> user_info_list) {
		this.user_info_list = user_info_list;
	}

	@Override
	public String toString() {
		return "BatchUserInfoResult [user_info_list=" + user_info_list + ", errcode=" + errcode + ", errmsg=" + errmsg
				+ "]";
	}
	
	
}
