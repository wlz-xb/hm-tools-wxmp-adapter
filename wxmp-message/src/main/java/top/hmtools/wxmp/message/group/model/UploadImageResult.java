package top.hmtools.wxmp.message.group.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class UploadImageResult extends ErrcodeBean {

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "UploadImageResult [url=" + url + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
