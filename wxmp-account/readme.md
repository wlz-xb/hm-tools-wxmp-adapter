#### 前言
本组件对应实现微信公众平台“账号管理”章节相关api接口，原接口文档地址：[账号管理](https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html)

#### 接口说明
- `top.hmtools.wxmp.account.apis.IQrcodeApis` 对应实现“带参数的二维码”相关接口。
- `top.hmtools.wxmp.account.apis.IShortUrlApis` 对应实现“长链接转短链接接口”相关接口。

#### 事件消息类说明
- `top.hmtools.wxmp.account.models.eventMessage`包下对应实现“微信认证事件推送”相关xml数据对应的Javabean数据结构。可参阅 top.hmtools.wxmp.account.enums.EQRActionName ， top.hmtools.wxmp.account.enums.EVerifyEventMessages

#### 使用示例
```
```
更多示例参见：
- [带参数的二维码](src/test/java/top/hmtools/wxmp/account/apis/IQrcodeApisTest.java)
- [长链接转短链接接口](src/test/java/top/hmtools/wxmp/account/apis/IShortUrlApisTest.java)