package top.hmtools.wxmp.account.apis;

import org.junit.Before;
import org.junit.Test;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.account.models.ShorturlParam;
import top.hmtools.wxmp.account.models.ShorturlResult;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class IShortUrlApisTest {
	
	protected WxmpSession wxmpSession;
	private IShortUrlApis shortUrlApis ;
	
	@Before
	public void init(){
		WxmpConfiguration wxmpConfiguration = new WxmpConfiguration();
		wxmpConfiguration.setAppid(AppId.appid);
		wxmpConfiguration.setAppsecret(AppId.appsecret);
		WxmpSessionFactoryBuilder builder = new WxmpSessionFactoryBuilder();
		WxmpSessionFactory factory = builder.build(wxmpConfiguration);
		this.wxmpSession = factory.openSession();
		shortUrlApis = this.wxmpSession.getMapper(IShortUrlApis.class);
	}

	@Test
	public void testGetShorUrl() {
		ShorturlParam shorturlParam = new ShorturlParam();
		shorturlParam.setAction("long2short");
		shorturlParam.setLong_url("https://3w.huanqiu.com/a/3458fa/7PhIUEtAEzS?agt=8");
		ShorturlResult shorUrl = this.shortUrlApis.getShorUrl(shorturlParam);
		System.out.println(shorUrl);
	}

}
