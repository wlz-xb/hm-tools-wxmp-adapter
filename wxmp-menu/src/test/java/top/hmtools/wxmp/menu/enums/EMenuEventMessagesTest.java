package top.hmtools.wxmp.menu.enums;

import org.junit.Test;

import com.github.jsonzou.jmockdata.JMockData;

import top.hmtools.wxmp.core.model.message.BaseMessage;

public class EMenuEventMessagesTest {

	@Test
	public void test() {
		EMenuEventMessages[] values = EMenuEventMessages.values();
		for(EMenuEventMessages item:values){
			BaseMessage message = (BaseMessage)JMockData.mock(item.getClassName());
			System.out.println(message);
			System.out.println(message.toXmlMsg());
		}
	}
	
}
