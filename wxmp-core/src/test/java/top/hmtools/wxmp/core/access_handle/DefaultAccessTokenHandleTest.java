package top.hmtools.wxmp.core.access_handle;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;

public class DefaultAccessTokenHandleTest extends BaseTest{

	@Test
	public void testGetAccessTokenString() {
		BaseAccessTokenHandle baseAccessTokenHandle = this.factory.getWxmpConfiguration().getAccessTokenHandle();
		DefaultAccessTokenHandle defaultAccessTokenHandle = (DefaultAccessTokenHandle)baseAccessTokenHandle;
		String accessTokenString = defaultAccessTokenHandle.getAccessTokenString();
		System.out.println(accessTokenString);
		
		this.factory.getWxmpConfiguration().setForceAccessTokenString("aaa");
		accessTokenString = defaultAccessTokenHandle.getAccessTokenString();
		System.out.println(accessTokenString);
		
		this.factory.getWxmpConfiguration().setForceAccessTokenString("");
		accessTokenString = defaultAccessTokenHandle.getAccessTokenString();
		System.out.println(accessTokenString);
	}

}
