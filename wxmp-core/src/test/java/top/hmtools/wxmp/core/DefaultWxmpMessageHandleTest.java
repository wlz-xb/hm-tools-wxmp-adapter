package top.hmtools.wxmp.core;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class DefaultWxmpMessageHandleTest {
	
	private DefaultWxmpMessageHandle  defaultWxmpMessageHandle;

	@Test
	public void testAddMessageMetaInfo() {
		this.defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();
		WxmpControllerTest wxmpControllerTest = new WxmpControllerTest();
		this.defaultWxmpMessageHandle.addMessageMetaInfo(wxmpControllerTest);
		System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getEventMap()));
		System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getMsgTypeMap()));
	}

	@Test
	public void testProcessXmlData() {
		this.testAddMessageMetaInfo();
		//验证基础消息
		String xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1348831860</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[this is a test]]></Content><MsgId>1234567890123456</MsgId></xml>";
		Object processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		System.out.println(processXmlData);
		
		//验证事件消息
		xml="<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1442401093</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[naming_verify_success]]></Event><ExpiredTime>1442401093</ExpiredTime></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		System.out.println(processXmlData);
		
		//验证暂未实现处理器的事件消息
		xml="<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[CLICK]]></Event><EventKey><![CDATA[EVENTKEY]]></EventKey></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		System.out.println(processXmlData);
		
		//验证不存在的xml消息
		xml="<xml></xml>";
		processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
		System.out.println(processXmlData);
	}

}
